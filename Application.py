import argparse
import os
from csvprocess import *
parser = argparse.ArgumentParser(description="Script de modification du format d'un .csv")
parser.add_argument("fichier", help="le chemin relatif ou absolu du fichier à traiter")
parser.add_argument("-d", "--startDelimiter", help="Délimiteur d'origine si ce n'est pas \"|\"", default="|")
parser.add_argument("-D", "--endDelimiter", help="Délimiteur cible si ce n'est pas \";\"", default=";")
parser.add_argument("-o", "--output", help="Fichier cible (par défaut : fichier.new.csv)")
args = parser.parse_args()
if (not os.path.exists(args.fichier)):
    print("Le fichier " + args.fichier + " n'existe pas :(")
    exit(1)
if (not os.path.isfile(args.fichier)):
    print(args.fichier + " n'est pas un fichier :(")
    exit(2)
fileContent = lectureFichier(args.fichier, args.startDelimiter)
if fileContent == None:
    print("Erreur de lecture :(")
    exit(3)
fileContent = modifColonnes(fileContent)
if args.output:
    fichierDesti = args.output
else:
    fichierDesti = args.fichier
if (ecritureFichier(fichierDesti, fileContent, args.endDelimiter)):
    print("Fichier écrit avec succès :)")
    exit(0)
else:
    print("Erreur d'écriture :(")
    exit(4)
