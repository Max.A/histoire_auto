import csv
import os
import datetime

def lectureFichier(nomFichier, delimiteur="|"):
    fileContent = ""
    with open(nomFichier) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        fileContent = [row for row in reader]
    return fileContent

def ecritureFichier(nomFichier, content, delimiteur=";"):
    nomNewFichier = nomFichier[:len(nomFichier)-3] + "new.csv"
    with open(nomNewFichier, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=delimiteur)
        writer.writerows(content)
        return True
    return False

def modifColonnes(content):
    newContent = [[0 for i in range(19)] for j in range(len(content))]
    for i in range(1, len(content)):
        newContent[i][0] = content[i][0]
        newContent[i][1] = content[i][11]
        newContent[i][2] = content[i][8]
        newContent[i][3] = content[i][9]
        date = datetime.datetime.strptime(content[i][5], "%Y-%m-%d")
        newContent[i][4] = date.strftime("%d/%m/%Y")
        newContent[i][5] = content[i][16]
        newContent[i][6] = content[i][10]
        newContent[i][7] = content[i][6]
        newContent[i][8] = content[i][3]
        newContent[i][9] = content[i][1]
        newContent[i][10] = content[i][2]
        newContent[i][11] = content[i][4]
        newContent[i][12] = content[i][7]
        newContent[i][13] = content[i][12]
        newContent[i][14] = content[i][13]
        newContent[i][15] = content[i][14]
        newContent[i][16] = content[i][15].split(', ')[0]
        newContent[i][17] = content[i][15].split(', ')[1]
        newContent[i][18] = content[i][15].split(', ')[2]
    newContent[0][0] = "adresse_titulaire"
    newContent[0][1] = "nom"
    newContent[0][2] = "prenom"
    newContent[0][3] = "immatriculation"
    newContent[0][4] = "date_immatriculation"
    newContent[0][5] = "vin"
    newContent[0][6] = "marque"
    newContent[0][7] = "denomination_commerciale"
    newContent[0][8] = "couleur"
    newContent[0][9] = "carrosserie"
    newContent[0][10] = "categorie"
    newContent[0][11] = "cylindree"
    newContent[0][12] = "energie"
    newContent[0][13] = "places"
    newContent[0][14] = "poids"
    newContent[0][15] = "puissance"
    newContent[0][16] = "type"
    newContent[0][17] = "variante"
    newContent[0][18] = "version"
    return newContent
